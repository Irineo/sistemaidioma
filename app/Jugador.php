<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    public $table = "jugadores";

    protected $fillable = [
        'nombre', 'materno', 'paterno',
    ];
}
