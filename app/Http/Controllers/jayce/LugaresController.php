<?php

namespace App\Http\Controllers\jayce;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\jayce\Lugar;
use App\User;

class LugaresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user = Auth::User();
        $rol = $user->roles->implode('name', ',');
       
        return view("jayce.lugares");
    }

    public function lugares(){
        $datos = Lugar::all();
        // print_r($datos);
        foreach ($datos as $key => $value) {
            # code...
            $datoslugares[] = [
                'id' => $value->id,
                'nombre' => $value->nombre,
                'cp' => $value->cp,
                'region' => $value->region,
                'ciudad' => $value->ciudad,
                'calle' => $value->calle,
                'acciones' => $value->id
            ];
        }
       // print_r($datoscosmeticos);
        return json_encode(["data"=>$datoslugares]);
    }

    public function destroy($id){
        //echo $id;
        $datos = Lugar::find($id)->delete();
        //$lugar = Lugar::find($id);
        //echo "Id de usuario: " . $lugar["user_id"];
        // $usuario = User::select('users.name')
        //                     ->join('users', 'lugares.user_id', '=', 'users.id')
        //                     ->where('lugares.user_id', '=', $lugar["user_id"])->get();
        // echo $usuario;
        return json_encode([
            "error" => false,
            "msg" => "Datos eliminados exitosamente"
        ]);
    }
}
