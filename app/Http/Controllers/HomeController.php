<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = Auth::User();
        $rol = $user->roles->implode('name', ',');
        switch ($rol) {
            case 'administrador':
                $mensaje = "Bienvenido Administrador";
                return view("home", compact('mensaje'));
                break;
            case 'maestro':
                $mensaje = "Bienvenido Maestro";
                return view("home", compact('mensaje'));
                break;
            case 'alumno':
                $mensaje = "Bienvenido Alumno";
                return view("home", compact('mensaje'));
                break;
        }

        return view('home');
    }
}
