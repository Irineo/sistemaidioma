<?php

namespace App\Http\Controllers\bere;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\bere\Cosmetico;

class CosmeticosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {  
        $user = Auth::User();
        $rol = $user->roles->implode('name', ',');
       // 
        return view("bere.cosmeticos");
    }


    public function cosmetico()
    {   
        $datos = Cosmetico::all();
        //print_r($datos);
        foreach ($datos as $key => $value) {
            # code...
            $datoscosmeticos[]=[
                'id' => $value->id,
                'nombre' => $value->nombre,
                'marca' => $value->marca,
                'tipo' => $value->tipo,
                'color' => $value->color,
                'precio' => $value->precio,
                'stockb' => $value->stockb
            ];
        }

        return json_encode(["data"=>$datoscosmeticos]);
    }

}
