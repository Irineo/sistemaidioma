<?php

namespace App\jayce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lugar extends Model
{
    use SoftDeletes;
    public $table = "lugares";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'nombre', 'cp', 'region', 'ciudad', 'calle'
    ];
}
