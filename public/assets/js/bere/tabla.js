var table = false;
$(document).ready(function(){
	table = $('#tabla_bere');

	// begin first table
	table.DataTable({
		responsive: true,
		searchDelay: 500,
		processing: true,
		serverSide: false,
		ajax: {
			url: 'datos',
			type: 'GET',
			data: {
				// parameters for custom backend script demo
				columnsDef: [
					'id', 'nombre',
					'marca', 'tipo', 'color',
					'precio', 'stockb'],
			},
		},
		columns: [
			{data: 'id'},
			{data: 'nombre'},
			{data: 'marca'},
			{data: 'tipo'},
			{data: 'color'},
			{data: 'precio'},
			{data: 'stockb'},
		//	{data: 'Actions', responsivePriority: -1},
		],
	});
});

// Funcion para mostrar los datos en un modal con el doble click
$(document).on("dblclick","table>tbody>tr",function(e){
	e.preventDefault();
	var tablabere=$('#tabla_bere').DataTable();
    var datoscosmeticos = tablabere.row( $(this).closest("tr") ).data();
	console.log(datoscosmeticos);
	$('#exampleModal').modal('show');
	$('#tag_id').text(datoscosmeticos.id);
	$('#tag_nombre').text(datoscosmeticos.nombre);
	$('#tag_marca').text(datoscosmeticos.marca);
	$('#tag_tipo').text(datoscosmeticos.tipo);
	$('#tag_color').text(datoscosmeticos.color);
	$('#tag_precio').text(datoscosmeticos.precio);
	$('#tag_stockb').text(datoscosmeticos.stockb);	
});
