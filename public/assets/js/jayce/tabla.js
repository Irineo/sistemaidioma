var table = false;
$(document).ready(function(){
	table = $('#dt_lugares');

	// begin first table
	table.DataTable({
		responsive: true,
		searchDelay: 500,
		processing: true,
        serverSide: false,
        "lengthMenu": [[15, -1], [15, "Todos"]],
        //language:{url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"},
		ajax: {
			url: 'datos-lugares',
			type: 'GET',
			data: {
				// parameters for custom backend script demo
				columnsDef: [
					'id', 'nombre',
					'cp', 'region', 'ciudad',
					'calle', 'acciones'],
			},
		},
		columns: [
			{data: 'id'},
			{data: 'nombre'},
			{data: 'cp'},
			{data: 'region'},
			{data: 'ciudad'},
			{data: 'calle'},
			{data: 'acciones', responsivePriority: -1},
		],
		columnDefs: [
			{
				targets: -1,
				title: 'acciones',
				orderable: false,
				render: function(data, type, full, meta) {
					return '\
					    \
						<a href="javascript:;" onclick="borrarLugar('+data+')" class="btn btn-sm btn-clean btn-icon" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
				},
			},
		],
	});
});


function borrarLugar(lugar){
    console.log(lugar)
    Swal.fire({
        title: "Eliminar lugar",
        text: "¿Esta seguro que desea eliminar el lugar?",
        icon: "warning",
        showCancelButton: "Cancelar",
        confirmButtonText: "Si, eliminar!"
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                url:"borrarlugar/" + lugar,
                method:"delete",
                dataType: 'json',
                data: {_token:$('#token').val()},
                success:function(respuesta){	
                  console.log(respuesta)
                },
                error: function(respuesta){
                  console.log(respuesta)
                 }
              })
              $('#dt_lugares').DataTable().ajax.reload();
            Swal.fire(
                "Eliminado!",
                "Los datos del lugar han sido eliminados exitosamente.",
                "success"
            )
        }
    }); 
}