<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {

	// Route::get('/', function () {
	//     return view('welcome');
	// });

	Route::get('/', 'HomeController@index');
	Route::get('/tablas', 'TablasController@index');
	/******************************/
	// Ejemplos
	//Rutas Bere
	Route::get('/cosmeticos', 'bere\CosmeticosController@index');
	Route::get('/datos', 'bere\CosmeticosController@cosmetico');
	//Rutas Jayce
	Route::get('/lugares', 'jayce\LugaresController@index');
	Route::get('/datos-lugares', 'jayce\LugaresController@lugares');
	Route::delete('borrarlugar/{id}','jayce\LugaresController@destroy');
});

Auth::routes();

