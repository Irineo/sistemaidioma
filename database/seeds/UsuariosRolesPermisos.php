<?php

use Illuminate\Database\Seeder;
use App\User;

class UsuariosRolesPermisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrador = User::create([
            'name' => 'UsuarioAdmin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $administrador->assignRole('administrador');
            
        $maestro = User::create([
            'name' => 'UsuarioMaestro',
            'email' => 'maestro@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $maestro->assignRole('maestro');

        $alumno = User::create([
            'name' => 'UsuarioAlumno',
            'email' => 'alumno@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $alumno->assignRole('alumno');
                
    }
}
