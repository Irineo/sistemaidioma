<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesPermisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permissions.cache');

        $permission = Permission::create(['name' => 'crear administrador']);
        $permission = Permission::create(['name' => 'leer administradores']);
        $permission = Permission::create(['name' => 'actualizar administrador']);
        $permission = Permission::create(['name' => 'eliminar administrador']);

        $permission = Permission::create(['name' => 'crear maestro']);
        $permission = Permission::create(['name' => 'leer maestros']);
        $permission = Permission::create(['name' => 'actualizar maestro']);
        $permission = Permission::create(['name' => 'eliminar maestro']);

        $permission = Permission::create(['name' => 'crear alumno']);
        $permission = Permission::create(['name' => 'leer alumnos']);
        $permission = Permission::create(['name' => 'actualizar alumno']);
        $permission = Permission::create(['name' => 'eliminar alumno']);
        
        $role = Role::create(['name' => 'administrador']);

        $role->givePermissionTo('crear administrador');
        $role->givePermissionTo('leer administradores');
        $role->givePermissionTo('actualizar administrador');
        $role->givePermissionTo('eliminar administrador');

        $role->givePermissionTo('crear maestro');
        $role->givePermissionTo('leer maestros');
        $role->givePermissionTo('actualizar maestro');
        $role->givePermissionTo('eliminar maestro');

        $role = Role::create(['name' => 'maestro']);
        $role->givePermissionTo('crear alumno');
        $role->givePermissionTo('leer alumnos');
        $role->givePermissionTo('actualizar alumno');
        $role->givePermissionTo('eliminar alumno');

        $role = Role::create(['name' => 'alumno']);

    }
}
