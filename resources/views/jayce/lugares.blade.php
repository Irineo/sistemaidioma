@extends('layouts.app')

@section('content')
<!--begin: Datatable-->
<input name="token" id="token" type="hidden" value="{{ csrf_token() }}">
<h1>Datos de lugares de la republica mexicana</h1>
 <table class="table table-bordered table-hover table-checkable" id="dt_lugares" style="margin-top: 13px !important">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nombre</th>
			<th>C.P.</th>
			<th>Region</th>
			<th>Ciudad</th>
            <th>Calle</th>
            <th>Acciones</th>
		</tr>
	</thead>
</table> 
<!--end: Datatable-->
@endsection