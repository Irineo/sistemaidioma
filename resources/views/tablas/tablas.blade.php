@extends('layouts.app')

@section('content')
<table class="table table-bordered table-hover table-checkable" id="dt_futbolistas" style="margin-top: 13px !important">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellido Paterno</th>
			<th>Apellido Materno</th>
			<th>Equipo</th>
			<th>Goles</th>
		</tr>
	</thead>
	<tbody>
		@foreach($jugadores as $jugador)
			<tr>
				<td>{{ $jugador->nombre }}</td>
				<td>{{ $jugador->paterno }}</td>
				<td>{{ $jugador->materno }}</td>
				<td>{{ $jugador->equipo }}</td>
				<td>{{ $jugador->goles }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endsection
@section('js')
<script src="{{ asset('assets/css/demo10/js/pages/crud/datatables/data-sources/ajax-server-side.js?v=7.0.4') }}"></script>
<script>
$(document).ready(function(){
	$("#dt_futbolistas").DataTable();
})
</script>
@endsection

